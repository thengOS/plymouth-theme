thengOS
===========

This is a Plymouth theme for thengOS inspired by Arch Breeze.

The script is insired by the script from the dark-arch theme. The spinner
image is taken from Arch Breeze splash screen.

The theme wasn't tested with all possible features Plymouth offers.
Please provide feedback or suggestions how to improve it.


Installation
------------

Once installed you can set it as your theme:

```
$ sudo plymouth-set-default-theme -R thengos
```


License
-------

MIT


Author
------

Sanoob Pattanath
